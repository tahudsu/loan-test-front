import axios from "axios";


class LoansServices { 
    async getLoanData() {
        const urlConstraints = `http://localhost:3000/application/constraints`;
        const config = {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Method": "OPTIONS"
            }
        };
        
        const data = await axios.get(urlConstraints, config)
            .then((data) => {
                console.log(data);
                return data.data;
            });
        /* const constraints = await axios.get(
            urlOffer, config
            ).then((data) => {
            const constraints = data;
            console.log(constraints);
            return constraints;
        }) */

        return { ...data };
    }

    async getAmountAndTerm(loanData) {
        console.log('from state', loanData);
        const urlOffer = `http://localhost:3000/application/offer?amount=${loanData.amountInterval.defaultValue}&term=${loanData.termInterval.defaultValue}`;
        const config = {
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Method": "OPTIONS"
            }
        };
        
        const data = await axios.get(urlOffer, config)
            .then((data) => {
                console.log(data);
                return data.data;
            });
        return { ...data };
    }
}

export default LoansServices;
