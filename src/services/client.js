import axios from "axios";

export class ClientService {
    getClient() {

    }

    async createClient(data) {
        const url = `http://localhost:3000/clients`;
        return await axios.post(url, data);
    }

    async createLoanForClient(data) {
        console.log(data);
        const headers = {
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
            "Access-Control-Allow-Origin": "*",
        }
        const url = `http://localhost:3000/clients/application?amount=${data.amountInterval.defaultValue}&term=${data.termInterval.defaultValue}`;
        await axios.post(url, '', { headers });
    }

    async login(data) {
        const dataToSend = {
            username: data.email,
            password: data.password
        }
        const url = `http://localhost:3000/login`;
        const options = {
            // withCredentials: true,
            headers: {
                "Access-Control-Allow-Origin": "*",
            }
        }

        const response = await axios.post(url, dataToSend, options)
        localStorage.setItem('token', response.data);
        return Promise.resolve(response);
    }

    getLoans() {
        const url = `http://localhost:3000/clients/loans`;
        const headers = {
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
            "Access-Control-Allow-Origin": "*",
        }
        return axios.get(url, { headers });
    } 
}