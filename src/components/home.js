import React from 'react';
import Slider from './slider';
import { Container, Row, Card, Button, ButtonToolbar } from 'react-bootstrap';
import LoansServices from '../services/loans';
import { Redirect, Link } from 'react-router-dom';

function ShowClientLoans(prop) {
    if (localStorage.getItem('token')) {
        return (
            <Link to='/showLoans'>
                <Button variant="secondary">Show Loans</Button>
            </Link>);
    }

    return null;
}

class Home extends React.Component {
    dataResolve = new LoansServices();
    constructor() {
        super();
        this.state = {
            amountInterval: {},
            termInterval: {},
            contractLoanData: {},
            viewForm: false
        }
        // this.dataResolve = new LoansServices();

        this.handleChange = this.handleChange.bind(this);
    }
    async componentDidMount() {
        const loanData = await this.dataResolve.getLoanData();
        const contractLoanData = await this.dataResolve.getAmountAndTerm(loanData);
        this.setState({
            amountInterval: loanData.amountInterval,
            termInterval: loanData.termInterval,
            contractLoanData,
        });
    }

    componentWillUnmount() {
        this.setState({
            viewForm: false
        })
    }

    async handleChange(value) {
        value.data.defaultValue = value.rangeVal;
        if (this.checkForAmount(value)) {
            this.setState({
                amountInterval: value.data
            });
        } else {
            this.setState({
                termInterval: value.data
            })
        }
        console.log(this.state);
        const res = await this.dataResolve.getAmountAndTerm(this.state);
        console.log(res);
        this.setState({
            contractLoanData: { ...res }
        })
    }

    checkForAmount(value) {
        return (value.data.min === this.state.amountInterval.min && value.data.max
            === this.state.amountInterval.max && value.data.step === this.state.amountInterval.step);
    }

    apply() {

        this.setState({
            viewForm: true
        })
    }


    render() {
        // this.loadData();
        const { amountInterval, termInterval } = this.state;

        if (this.state.viewForm) {
            console.log('preview state until location', this.state);

            return (
                <Redirect to={{
                    pathname: "/clientForm",
                    state: { loans: this.state }
                }} />
            )
        }

        return (
            <Container>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Title>Online Credit</Card.Title>
                        <label name="amount">How do you need?</label>
                        <Slider value={amountInterval} change={this.handleChange}></Slider>
                        <label name="term">How many days are you going to refund?</label>
                        <Slider value={termInterval} change={this.handleChange}></Slider>
                    </Card.Body>
                    <hr></hr>
                    <Card.Body>
                        <Card.Title>Result Data</Card.Title>
                        <p>Principal : {this.state.contractLoanData.principalAmount}</p>
                        <p>interestAmount : {this.state.contractLoanData.interestAmount}</p>
                        <p>totalAmount : {this.state.contractLoanData.totalAmount}</p>
                        <p>expiration date: {this.state.contractLoanData.dueDate}</p>
                        <ButtonToolbar>
                            <Button variant="primary" onClick={() => this.apply()}>
                                Apply
                            </Button>
                            <ShowClientLoans></ShowClientLoans>
                        </ButtonToolbar>
                    </Card.Body>
                </Card>
            </Container>
        );
    }
}

export default Home;