import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { ClientService } from '../services/client';

class ClientForm extends Component {

    clientService = new ClientService();
    constructor(props) {
        super(props);
        this.change = this.change.bind(this);
    }

    async handleSubmit(e) {
        e.preventDefault();
        const created = await this.clientService.createClient(this.state);
        if (created.status === 201) {
            const login = await this.clientService.login(this.state);
            if (login) {
                await this.clientService.createLoanForClient(this.props.location.state.loans);
            }
        }
    }

    change(e) {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    render() {
        console.log(this.props.location.state);

        return (
            <Form onSubmit={ (e) => this.handleSubmit(e) }>
                <Form.Group controlId="email">
                    <Form.Label>
                        email
                    </Form.Label>
                    <Form.Control type="email" placeholder="Enter email" onChange={ this.change }>

                    </Form.Control>
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else. 
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="name">
                    <Form.Label>
                        Name
                    </Form.Label>
                    <Form.Control type="text" placeholder="client name" onChange={ this.change }>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="surname">
                    <Form.Label>
                        Surname
                    </Form.Label>
                    <Form.Control type="text" placeholder="client surname" onChange={ this.change }>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="personalId">
                    <Form.Label>
                        personalId
                    </Form.Label>
                    <Form.Control type="number" maxLength="11" placeholder="client personalId" onChange={ this.change }>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={ this.change }/>
                </Form.Group>
                <Form.Group controlId="formBasicPassword2">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={ this.change }/>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        )
    }
}

export default ClientForm;