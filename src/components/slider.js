import React from 'react';
import { Row } from 'react-bootstrap';

  function Range(props) {
      return (
        <Row>
          <input id="range" type="range"
            value={ props.data.defaultValue }
            min={props.data.min}
            max={props.data.max}
            step={props.data.step}
            onChange={(e) => props.updateRange(e.target.value)}
          />
          <span id="output">{  props.data.defaultValue }</span>
        </Row>
      )
  }
  
  class Slider extends React.Component {
    constructor() {
      super();
      const data = {
        defaultValue: '',
        min: 0,
        max: 0,
        step: 0
      }
      this.state = {
        rangeVal: 0,
        data
      };
      this.updateRange = this.updateRange.bind(this);
    }

   

    /* componentWillUpdate() {
      console.log('will update', this.props);
    } */
    
    updateRange(val) {
        console.log(val);
        // debugger
      const defaultValue =  val;
      console.log('update val', this.state.data);
      this.setState({
        rangeVal: val,
        data: { ...this.state.data, defaultValue } 
      });
      console.log('preview', this.state);
      this.props.change(this.state);
    } 

    componentWillReceiveProps(nextProps) {
      this.initSlider();
    }
    
    initSlider() {
      this.setState((state, props) => {
        return { data: props.value };
      })
    }

    render() {
      const { rangeVal, data } = this.state;
      return (
        <Range range={rangeVal} updateRange={this.updateRange} data={data} />
      )
    }  
  }

export default Slider;