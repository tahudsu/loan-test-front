import React, { Component } from "react";
import { ClientService } from "../services/client";

export class ClientLoans extends Component {
    clientService = new ClientService();

    constructor() {
        super();
        this.state = {
            loans: []
        }
    }
    
    async UNSAFE_componentWillMount() {
        if (localStorage.getItem('token')) {
            const loans = await this.clientService.getLoans();
            this.setState({
                loans: loans.data._embedded.loans
            })
        }
        
    }

    render() {

        if (this.state.loans.length) {
            return (
                <h1>There are Loans</h1>
            )
        } else {
            return (
                <h1>there aren't loans active</h1>
            )
        }

    }
}