import React from 'react';
import { Link, Switch, Route, BrowserRouter } from 'react-router-dom';
import Home from './components/home';
import { Button, Container, Row } from 'react-bootstrap';
import ClientForm from './components/clientForm';
import { ClientLoans } from './components/clientLoans';

class Routing extends React.Component {
    

    render() {
        return (
            <BrowserRouter>
                <Container>
                    <Row>
                        <ul>
                            <li>
                                <Link to='/'>
                                    <Button>Home</Button>
                                </Link>
                                <Link to='/clientForm'>
                                    <Button>ClientForm</Button>
                                </Link>
                            </li>
                        </ul>
                    </Row>
                    <Row>
                        <Switch>
                                <Route path="/clientForm" component={ClientForm}>
                                </Route>
                                <Route path='/showLoans' component={ClientLoans}>
                                </Route>
                                <Route path="/">
                                    <Home />
                                </Route>
                                
                        </Switch>
                    </Row>
                </Container>
            </BrowserRouter>
        );
    }
}

export default Routing;