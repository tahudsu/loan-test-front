import React from 'react';
import logo from './logo.svg';
// import './App.css';
import Routing from './routing';
import { Container } from 'react-bootstrap';

function App() {
  return (
    <Container className="App">
      <Routing>
      </Routing>
    </Container>
  );
}

export default App;
