FROM node:8.15.0-alpine

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.2.1/wait /wait
RUN npm install -g http-server
RUN chmod +x /wait

#COPY www /www/app

 
#WORKDIR /www/app

EXPOSE 8083 

#CMD /wait && http-server
CMD /wait && npm start